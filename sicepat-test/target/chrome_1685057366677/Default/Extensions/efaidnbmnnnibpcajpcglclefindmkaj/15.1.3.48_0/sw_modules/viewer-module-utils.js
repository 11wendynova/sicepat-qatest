/*************************************************************************
* ADOBE CONFIDENTIAL
* ___________________
*
*  Copyright 2015 Adobe Systems Incorporated
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Adobe Systems Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Adobe Systems Incorporated and its
* suppliers and are protected by all applicable intellectual property laws,
* including trade secret and or copyright laws.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Adobe Systems Incorporated.
**************************************************************************/
import{common as e}from"./common.js";import{util as t}from"./util.js";import{SETTINGS as a}from"./settings.js";import{dcLocalStorage as o}from"../common/local-storage.js";import{floodgate as s}from"./floodgate.js";import{loggingApi as i}from"../common/loggingApi.js";let n;n||(n=new function(){this.updateVariables=function(n){try{let l=0!=n&&1!=n&&-1!=n,r=!(!l||n===a.READER_VER||n===a.ERP_READER_VER);o.setItem("locale",t.getFrictionlessLocale(chrome.i18n.getMessage("@@ui_locale"))),o.setItem("cdnUrl",e.getAcrobatViewerUri()),o.setItem("isDeskTop",l),o.setItem("env",e.getEnv()),o.setItem("viewerImsClientId",e.getViewerIMSClientId()),o.setItem("imsContextId",e.getImsContextId()),o.setItem("viewerImsClientIdSocial",e.getViewerIMSClientIdSocial()),o.setItem("imsURL",e.getIMSurl()),o.setItem("imsLibUrl",e.getImsLibUrl()),o.setItem("dcApiUri",e.getDcApiUri()),o.setItem("isAcrobat",r);let c=[this.checkFeatureEnable({flagName:"dc-cv-modern-viewer",storageKey:"modernViewerEnable"}),this.checkFeatureEnable({flagName:"dc-cv-dark-mode",storageKey:"darkModeEnable"}),this.checkFeatureEnable({flagName:"dc-cv-read-aloud",storageKey:"isReadAloudEnable"}),this.checkFeatureEnable({flagName:"dc-cv-use-old-domain",storageKey:"oldDomainRollback"}),this.checkFeatureEnable({flagName:"dc-cv-save-location-on-options",storageKey:"isSaveLocationPrefEnabled"}),this.checkFeatureEnable({flagName:"dc-cv-openInDesktop-hotfix"}),this.checkFeatureEnable({flagName:"dc-cv-enable-splunk-logging",storageKey:"splunkLoggingEnable"}),this.checkFeatureEnable({flagName:"dc-cv-extension-menu",storageKey:"enableNewExtensionMenu"})];return navigator.onLine&&c.push(this.checkFeatureEnable({flagName:"dc-cv-offline-support-disable",storageKey:"offlineSupportDisable"})),Promise.all(c).then((([e,a,r,c,m,g,d,I])=>{if(o.getItem("theme")&&!a?o.removeItem("theme"):a&&!o.getItem("theme")&&o.setItem("theme","auto"),!m&&o.getItem("saveLocation")?o.removeItem("saveLocation"):m&&!o.getItem("saveLocation")&&o.setItem("saveLocation","ask"),g&&!t.isEdge()&&(l=n>0,o.setItem("isDeskTop",l)),i.registerLogInterval(d),d){let e=s.getFeatureMeta("dc-cv-enable-splunk-logging");e=JSON.parse(e),o.setItem("allowedLogIndex",e.index)}t.enableNewExtensionMenu(I)}))}catch(e){}},this.checkFeatureEnable=async function(e){const{flagName:t,storageKey:a}=e,i=await s.hasFlag(t);return a&&o.setItem(a,!!i),i}});export const viewerModuleUtils=n;